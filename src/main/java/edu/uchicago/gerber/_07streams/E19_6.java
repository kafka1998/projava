package edu.uchicago.gerber._07streams;
import java.util.Currency;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class E19_6 {
    public static void turnSteam(Set<Currency> currenciesSet) {
        Stream<Currency> currencyStream = currenciesSet.stream();
        currencyStream.map(Currency::getDisplayName).sorted().forEach(System.out::println);
    }

    public static void main(String[] args) {
        Set<Currency> currenciesSet = Currency.getAvailableCurrencies();
        turnSteam(currenciesSet);
    }

}
