package edu.uchicago.gerber._07streams;

public class E19_14 {
    public static String toBinary(int number) {
        if (number <= 1) {
            return number + "";
        } else {
            return toBinary(number / 2) + (number % 2);
        }
    }

}
