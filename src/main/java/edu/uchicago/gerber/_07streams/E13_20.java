package edu.uchicago.gerber._07streams;
import java.util.HashSet;
import java.util.Set;

public class E13_20 {
    /**
     * E13.20
     */
    public static void main(String[] args) {
        Set<String> possibleSet = new HashSet<String>();
        getPossibles(possibleSet, 50, 0, 0, 0, 0);
        possibleSet.forEach(s -> System.out.println(s));
    }

    public static void getPossibles(Set<String> possibleSet, int price, int num_1, int num_5,
                                    int num_20, int num_100) {
        if (price == 0) {
            possibleSet.add("$1 Number: " + num_1 + " $5 Number: " + num_5 + " $20 Number: " + num_20 + " $100 Number: " + num_100);
        } else if (price > 0) {
            getPossibles(possibleSet, price - 1, num_1 + 1, num_5, num_20, num_100);
            getPossibles(possibleSet, price - 5, num_1, num_5 + 1, num_20, num_100);
            getPossibles(possibleSet, price - 20, num_1, num_5, num_20 + 1, num_100);
            getPossibles(possibleSet, price - 100, num_1, num_5, num_20, num_100 + 1);
        }
    }

}
