package edu.uchicago.gerber._07streams;

import java.util.stream.Collectors;
import java.util.stream.Stream;

public class E19_5 {
    public static <T> String toString(Stream<T> stream, int n) {
        return stream.limit(n).map(Object::toString).collect(Collectors.joining(","));
    }

}
