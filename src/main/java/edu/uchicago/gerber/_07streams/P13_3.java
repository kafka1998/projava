package edu.uchicago.gerber._07streams;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class P13_3 {

    private static String[][] phoneNumbers = {
            {"A", "B", "C"},
            {"D", "E", "F"},
            {"G", "H", "I"},
            {"J", "K", "L"},
            {"M", "N", "O"},
            {"P", "Q", "R", "S"},
            {"T", "U", "V"},
            {"W", "X", "Y", "Z"}
    };

    public static List<String> getList(String path) {
        try (Stream<String> lines = Files.lines(Paths.get(path), StandardCharsets.UTF_8)) {
            return lines.collect(Collectors.toList());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void main(String[] args) {
        String filePath = "words.txt";
        List<String> words = getList(filePath);
        if (words == null) {
            return;
        }
        String number = "2633465282";
        possibleWord(number, "", words);

    }

    private static void possibleWord(String num, String word, List<String> words) {
        if (num.length() == 0) {
            if (words.contains(word)) {
                System.out.println(word);
            }
        } else {
            for (int i = 0; i < num.length(); i++) {
                int index = Integer.parseInt(num.substring(i, i + 1));
                String[] letterArray = phoneNumbers[index];
                for (int j = 0; j < letterArray.length; j++) {
                    String result = word + letterArray[j];
                    possibleWord(num.substring(1), result, words);
                }
            }
        }
    }

}
