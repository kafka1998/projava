package edu.uchicago.gerber._07streams;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class E19_16 {
    public static List<String> getList(String path) {
        try (Stream<String> lines = Files.lines(Paths.get(path), StandardCharsets.UTF_8)) {
            return lines.collect(Collectors.toList());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void main(String[] args) {
        String filePath = "warAndPeace.txt";
        List<String> words = getList(filePath);
        if (words == null) {
            return;
        }
        Stream<String> stream = words.stream();
        Map<String, Double> averageMap = stream
                .filter(s -> s.length() > 0)
                .map(String::toLowerCase)
                .collect(Collectors.groupingBy(s -> s.substring(0, 1),
                        Collectors.averagingDouble(String::length)));
        averageMap.forEach((key, value) -> System.out.printf("%s : %.2f%n", key, value));
    }

    private static boolean isPalindromeStr(String s) {
        int length = s.length();
        char[] charArray = s.toCharArray();
        for (int i = 0; i < length / 2; i++) {
            if (charArray[i] != charArray[length - i - 1]) {
                return false;
            }
        }
        return true;
    }

}
