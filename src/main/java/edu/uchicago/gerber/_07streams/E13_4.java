package edu.uchicago.gerber._07streams;

public class E13_4 {
    public static String toBinary(int number) {
        if (number <= 1) {
            return number + "";
        } else {
            return toBinary(number / 2) + (number % 2);
        }
    }
}
