package edu.uchicago.gerber._07streams;

public class YodaSpeak {
    public static String reverse(String sentence) {
        String reversedStr = "";
        String[] words = sentence.split("\\s+");
        int length = words.length;
        for (int i = length - 1; i >= 0; i--) {
            reversedStr += words[i] + " ";
        }
        return reversedStr;
    }

    public static void main(String[] args) {
        String sentence = "The force is strong with you";
        System.out.println(reverse(sentence));
    }
}
