package edu.uchicago.gerber._07streams;

public class YodaSpeakRecursive {
    public static String reverseRecursive(String sentence, String reverseStr) {
        if (sentence.isEmpty()) {
            return reverseStr;
        }
        int index = 0;
        while (index < sentence.length() && sentence.charAt(index) != ' ') {
            index++;
        }
        reverseStr = sentence.substring(0, index) + " " + reverseStr;
        if (index < sentence.length()) {
            sentence = sentence.substring(index + 1);
        } else {
            sentence = sentence.substring(index);
        }
        return reverseRecursive(sentence, reverseStr);
    }

    public static void main(String[] args) {
        String sentence = "The force is strong with you";
        System.out.println(reverseRecursive(sentence, ""));
    }
}
