package edu.uchicago.gerber._07streams;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Currency;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;


public class E19_7 {
    public static List<String> turnString(List<String> stringList) {
        Stream<String> stream = stringList.stream();
        return stream.filter(s -> s.length() >= 2).map(s -> {
            String firstLetter = s.substring(0, 1);
            String lastLetter = s.substring(s.length() - 1);
            return firstLetter + "..." + lastLetter;
        }).collect(Collectors.toList());
    }

    public static void main(String[] args) {
        List<String> list = Arrays.asList("Write", "a","bs","day","help","WORD","c");
        List<String> result = turnString(list);
        System.out.println(result);
    }

}
