package edu.uchicago.gerber._06design.P12_1;


import java.util.*;
public class VendingTest {
    static void printMenu()
    {
        System.out.println("\n0. Quit");
        System.out.println("1. Product list");
        System.out.println("2. Buy product");
        System.out.println("3. Add product(operator only)");
        System.out.println("4. Remove money(operator only)");
        System.out.print("Enter number:  ");

    }

    static void printPorducts(VendingMachine machine)
    {
        for (Map.Entry<String, Double> set : machine.productList().entrySet())
        {
            // Printing all elements of a Map
            String pName = set.getKey();
            System.out.println(String.format("%s: %.2f (%d left)", pName, set.getValue(), machine.getStock().get(pName)));
        }
    }

    public static void main(String[] args) {

        VendingMachine machine = new VendingMachine();

        machine.addProduct("coke", 2.0, 5);
        machine.addProduct("water", 1.0, 3);
        machine.addProduct("cookie", 3.0, 5);
        machine.loadCoins(10,10,10,30);

        System.out.println("Welcome to Vending machine");

        while (true)
        {
            printMenu();
            Scanner scan = new Scanner(System.in);
            int choice = scan.nextInt();

            switch (choice)
            {
                case 0:
                    return;

                case 1:
                    System.out.println();
                    printPorducts(machine);
                    break;

                case 2:
                    System.out.println();
                    printPorducts(machine);
                    System.out.println();
                    System.out.print("Enter the cents you want to put: ");
                    int cents = scan.nextInt();
                    System.out.print("Enter the dimes you want to put: ");
                    int dimes = scan.nextInt();
                    System.out.print("Enter the quarters you want to put: ");
                    int quarters = scan.nextInt();
                    System.out.print("Enter the dollars you want to put: ");
                    int dollars = scan.nextInt();
                    machine.addCoins(cents, dimes, quarters, dollars);
                    System.out.println();
                    System.out.print("Enter the name of product: ");
                    scan = new Scanner(System.in);
                    String pName = scan.nextLine();
                    int result = machine.purchase(pName);
                    if (result == 1)
                        System.out.println("Declined ! There is no stock.");
                    if (result == 2)
                        System.out.println("Declined ! Not enough money.");
                    if (result == 0)
                    {
                        Coins change = machine.getChange();
                        System.out.println(String.format("Success ! Your change is:  Cents: %d, Dime: %d, Quarter: %d, Dollar: %d",
                                change.getCent(), change.getDime(), change.getQuarter(), change.getDollar()));
                    }
                    break;

                case 3:
                    System.out.println();
                    scan = new Scanner(System.in);
                    System.out.print("Enter the name of product: ");
                    String name = scan.nextLine();
                    System.out.print("Enter the prize: ");
                    double prize = scan.nextDouble();
                    System.out.print("Enter the number of products you added: ");
                    int num = scan.nextInt();
                    machine.addProduct(name, prize, num);
                    break;

                case 4:
                    System.out.println(String.format("\nThe Balance : %.2f", machine.getBalance().getValue()));
                    System.out.println(String.format("Cents: %d, Dime: %d, Quarter: %d, Dollar: %d",
                            machine.getBalance().getCent(), machine.getBalance().getDime(),
                            machine.getBalance().getQuarter(), machine.getBalance().getDollar()));
                    scan = new Scanner(System.in);
                    System.out.print("Enter the cents you want to take: ");
                    int cents2 = scan.nextInt();
                    System.out.print("Enter the dimes you want to take: ");
                    int dimes2 = scan.nextInt();
                    System.out.print("Enter the quarters you want to take: ");
                    int quarters2 = scan.nextInt();
                    System.out.print("Enter the dollars you want to take: ");
                    int dollars2 = scan.nextInt();
                    machine.removeMoney(cents2, dimes2, quarters2, dollars2);
                    System.out.println(String.format("Cents: %d, Dime: %d, Quarter: %d, Dollar: %d",
                            machine.getBalance().getCent(), machine.getBalance().getDime(),
                            machine.getBalance().getQuarter(), machine.getBalance().getDollar()));
                    break;
            }


        }
    }



}
