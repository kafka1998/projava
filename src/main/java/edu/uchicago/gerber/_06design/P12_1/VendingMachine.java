package edu.uchicago.gerber._06design.P12_1;
import java.util.*;


public class VendingMachine {
    private Map<String, Double> products;
    private Map<String, Integer> stock;
    private Coins balance;
    private Coins buyBalance;
    private Coins change;

    public VendingMachine()
    {
        this.products = new HashMap<String, Double>();
        this.stock = new HashMap<String, Integer>();
        this.balance = new Coins(0,0,0,0);
        this.buyBalance = new Coins(0,0,0,0);
        this.change = new Coins(0,0,0,0);
    }

    public void addProduct(String name, double value, int num)
    {
        if (! products.containsKey(name))
            this.products.put(name, value);
        this.stock.put(name, num);
    }


    public void loadCoins(int cent, int dime, int quarter, int dollar)
    {
        this.balance.loadMoney(cent,dime,quarter,dollar);
    }

    public void removeMoney(int cent, int dime, int quarter, int dollar)
    {
        this.balance.removeMoney(cent,dime,quarter,dollar);
    }

    public void addCoins(int cent, int dime, int quarter, int dollar)
    {
        this.buyBalance.loadMoney(cent,dime,quarter,dollar);
    }

    public int purchase(String product)
    {
        if (! this.stock.containsKey(product) || this.stock.get(product) <= 0)
            return 1;
        if (this.buyBalance.getValue() < this.products.get(product))
            return 2;
        this.stock.put(product,this.stock.get(product) - 1);
        double changeAmount = this.buyBalance.getValue() - this.products.get(product);
        this.change = convertToCoins(changeAmount);

        this.balance.mergeCoins(this.buyBalance);

        this.buyBalance = new Coins(0,0,0,0);

        this.balance.removeCoins(this.change);
        return 0;
    }

    public Coins getChange()
    {
        return this.change;
    }

    private Coins convertToCoins(double changeDue)
    {
        int change = (int)(Math.ceil(changeDue*100));
        int dollars = Math.round((int)change/100);
        change=change%100;
        int quarters = Math.round((int)change/25);
        change=change%25;
        int dimes = Math.round((int)change/10);
        change=change%10;
        int cents = Math.round((int)change/1);
        Coins coins = new Coins(cents,dimes,quarters,dollars);
        return coins;
    }


    public Map<String, Double> productList()
    {
        return this.products;
    }

    public Map<String, Integer> getStock()
    {
        return this.stock;
    }

    public Coins getBalance() {
        return this.balance;
    }
}
