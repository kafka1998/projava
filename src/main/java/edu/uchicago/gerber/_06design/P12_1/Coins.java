package edu.uchicago.gerber._06design.P12_1;

public class Coins {
    private int cent;
    private int dime;
    private int quarter;
    private int dollar;

    public Coins(int cent, int dime, int quarter, int dollar)
    {
        this.cent = cent;
        this.dollar = dollar;
        this.dime = dime;
        this.quarter = quarter;
    }


    public double getValue()
    {
        return cent * 0.01 + dime * 0.1 + quarter * 0.25 + dollar * 1;
    }


    public void loadMoney(int cent, int dime, int quarter, int dollar)
    {
        this.cent += cent;
        this.dollar += dollar;
        this.dime += dime;
        this.quarter += quarter;
    }


    public void removeMoney(int cent, int dime, int quarter, int dollar)
    {
        this.cent -= cent;
        this.dollar -= dollar;
        this.dime -= dime;
        this.quarter -= quarter;
    }

    public void mergeCoins(Coins c)
    {
        this.cent += c.cent;
        this.dollar += c.dollar;
        this.dime += c.dime;
        this.quarter += c.quarter;
    }

    public void removeCoins(Coins c)
    {
        this.cent -= c.cent;
        this.dollar -= c.dollar;
        this.dime -= c.dime;
        this.quarter -= c.quarter;
    }

    public int getCent() {
        return cent;
    }

    public int getDime() {
        return dime;
    }

    public int getDollar() {
        return dollar;
    }

    public int getQuarter() {
        return quarter;
    }
}

