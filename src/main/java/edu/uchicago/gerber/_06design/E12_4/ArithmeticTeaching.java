package edu.uchicago.gerber._06design.E12_4;
import java.util.Random;
import java.util.Scanner;


public class ArithmeticTeaching {
    private int level = 1;
    private int score = 0;
    private Random rand;

    public ArithmeticTeaching()
    {
         rand= new Random();
    }

    public void teach()
    {
        System.out.println(String.format("Welcome to arithmetic level %d teaching!", this.level));
        while (level <= 3)
        {
            while (this.score < 5) {
                System.out.println(String.format("\nCurrent Score: %d", this.score));
                switch (level) {
                    case 1:
                        level1_problem();
                        break;
                    case 2:
                        level2_problem();
                        break;
                    case 3:
                        level3_problem();
                        break;
                }
            }

            // End of this level teaching
            this.score = 0;
            this.level ++;
            System.out.println("You are move to the next level! \n");
        }
    }


    private void validate(int result)
    {
        Scanner scan = new Scanner(System.in);
        System.out.print("Enter your answer: ");
        int answer = scan.nextInt();
        if (answer == result)
        {
            System.out.println("Correct !");
            this.score ++;
        }
        else
        {
            System.out.println("Wrong, you still have one more chance.");
            System.out.print("Enter your answer: ");
            answer = scan.nextInt();
            if (answer == result)
            {
                this.score ++;
                System.out.println("Correct !");
            }
            else
                System.out.println("You have used all chances, please answer new question.");
        }

    }
    public void level1_problem()
    {
        int num1 = rand.nextInt(10);
        int num2 = rand.nextInt(10 - num1);
        System.out.println(String.format("Question: %d + %d = ?", num1, num2));
        validate(num1 + num2);
    }

    public void level2_problem()
    {
        int num1 = rand.nextInt(10);
        int num2 = rand.nextInt(10);
        System.out.println(String.format("Question: %d + %d = ?", num1, num2));
        validate(num1 + num2);
    }


    public void level3_problem()
    {
        int num1 = rand.nextInt(10);
        int num2 = rand.nextInt(num1 + 1);
        System.out.println(String.format("Question: %d - %d = ?", num1, num2));
        validate(num1 - num2);
    }


}
