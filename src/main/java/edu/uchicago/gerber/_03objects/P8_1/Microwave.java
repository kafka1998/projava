package edu.uchicago.gerber._03objects.P8_1;

public class Microwave {
    private int time = 0;
    private int level = 1;

    public Microwave()
    {
        System.out.println("A Microwave is created.");
    }

    public void increase_time()
    {
        time += 30;
        System.out.println("Already cooked " + time + " seconds.");
    }

    public void switch_button()
    {
        if (level == 1)
            level = 2;
        else level = 1;
        System.out.println("Switch to level " + level);
    }

    public void reset()
    {
        time = 0;
        level = 1;
        System.out.println("The Microwave is reset");
    }

    public void start()
    {
        System.out.println("Cooking for " + time + " seconds at level " + level);
    }
}
