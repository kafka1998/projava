package edu.uchicago.gerber._03objects.P8_1;

public class MicrowaveTester {
    public static void main(String[] args) {

        //Create a microwave and start cooking for 150 seconds and push switch button
        //Then cook for another 150 seconds and reset.

        Microwave microw = new Microwave();

        microw.start();

        for (int i = 0; i < 5; i++)
        {
            microw.increase_time();
        }

        microw.start();
        microw.switch_button();

        for (int i = 0; i < 5; i++)
        {
            microw.increase_time();
        }

        microw.start();
        microw.reset();
        microw.start();
    }
}
