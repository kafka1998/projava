package edu.uchicago.gerber._03objects.P8_19;

import java.util.Scanner;

public class CannonballTester {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Input the angle (degree): ");
        double angle = in.nextDouble();
        System.out.print("Input the velocity: ");
        double vel = in.nextDouble();

        Cannonball ball = new Cannonball(0);
        ball.shoot(angle, vel);

    }
}
