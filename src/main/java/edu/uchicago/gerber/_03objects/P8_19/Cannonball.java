package edu.uchicago.gerber._03objects.P8_19;

public class Cannonball {
    private double x_position, y_position;
    private double velocity, alpha;
    private double x_velocity, y_velocity;



    public Cannonball(double xPos)
    {
        x_position = xPos;
        y_position = 0;
    }

    public void move(double sec)
    {
        x_position += x_velocity * sec;

        y_position += y_velocity * sec - 0.5 * sec * 9.81 * sec;
        y_velocity -= sec * 9.81;
//        System.out.println(y_velocity);
    }

    public double getX_position()
    {
        return x_position;
    }

    public double getY_position()
    {
        return y_position;
    }

    public void shoot(double angle, double vel)
    {
        alpha = angle;
        velocity = vel;
        x_velocity = velocity * Math.cos(Math.toRadians(alpha));
        y_velocity = velocity * Math.sin(Math.toRadians(alpha));


        double timer = 0.0;
        while (y_position >= 0)
        {
            System.out.print("At time " + timer + ": ");
            this.status();
            this.move(0.1);
            timer += 0.1;
        }
    }

    public void status()
    {
        System.out.println("The ball is now at X: " + getX_position() + ", Y: " + getY_position());
    }
}
