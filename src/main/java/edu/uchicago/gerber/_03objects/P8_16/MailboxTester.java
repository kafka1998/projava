package edu.uchicago.gerber._03objects.P8_16;

public class MailboxTester {
    public static void main(String[] args) {
        Message message1 = new Message("Bob","David");
        Message message2 = new Message("Kobe", "Messi");
        message1.append("Hi:");
        message1.append("Miss you.");
        message2.append("Hello:");
        message2.append("Miss you.");

        Mailbox box = new Mailbox();
        box.addMessage(message1);
        box.addMessage(message2);
        System.out.println(box.getMessage(0).toString());
        System.out.println(box.getMessage(1).toString());
        box.removeMessage(0);
        System.out.println(box.getMessage(0).toString());


    }
}
