package edu.uchicago.gerber._03objects.P8_16;

public class Message {
    private String recipient;
    private String sender;
    private String text;


    public Message(String recipName, String senderName)
    {
        recipient = recipName;
        sender = senderName;
        text = "";
    }

    public void append(String line)
    {
        text = text + line + "\n";
    }

    public String toString()
    {
        return String.format("From: %s\nTo: %s\n%s",sender, recipient,text);
    }
}
