package edu.uchicago.gerber._03objects.P8_6;

public class CarTester {
    public static void main(String[] args) {
        //Create a car with 50 miles per gallon efficiency
        //Then add 20 gallon
        //Try to drive 1001 miles and expect the failure
        //Then drive 100 miles and report the tank level


        System.out.println("Create a new care with 50 miles per gallon efficiency");
        Car myHybrid = new Car(50); // 50 miles per gallon
        myHybrid.addGas(20); // Tank 20 gallons
        System.out.println("Adding 20 gallon. \n Try to drive 1001 miles");
        myHybrid.drive(1001);
        System.out.println("Drive 100 miles.");
        myHybrid.drive(100); // Drive 100 miles
        System.out.println("Remaining gallons: " + myHybrid.getGasLevel()); // Print fuel remaining
    }
}
