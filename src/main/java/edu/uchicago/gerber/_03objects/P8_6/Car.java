package edu.uchicago.gerber._03objects.P8_6;

public class Car {
    private double efficiency;
    private double fuel;

    public Car(double effi)
    {
        efficiency = effi;
        fuel = 0;
    }

    public void drive(double miles)
    {
        double cost_fuel = miles/efficiency;
        if (cost_fuel > fuel)
            System.out.println("Fuel is too less to drive so long distance.");
        else
            fuel = fuel - cost_fuel;
    }

    public void addGas(double gallon)
    {
        fuel += gallon;
    }

    public double getGasLevel()
    {
        return fuel;
    }

}
