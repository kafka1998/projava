package edu.uchicago.gerber._03objects.P8_8;

public class VotingTester {
    public static void main(String[] args) {
        //Create a new voting machine and let 10 vote for democrat and 5 vote for republican
        System.out.println("Create a new voting machine and let 10 vote for democrat and 5 vote for republican");
        VotingMachine vote = new VotingMachine();

        for(int i = 0; i < 10; i++)
        {
            vote.voteDemocrat();
        }

        for(int i = 0; i < 5; i++)
        {
            vote.voteRepublican();
        }

        vote.showTallies();


        //Clear and show results
        System.out.println("Clear the state and show results:");
        vote.clear();

        vote.showTallies();
    }
}
