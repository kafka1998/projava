package edu.uchicago.gerber._03objects.P8_8;

public class VotingMachine {
    private int democratCount, republicanCount;

    public VotingMachine()
    {
        democratCount = 0;
        republicanCount = 0;
    }

    public void clear()
    {
        democratCount = 0;
        republicanCount = 0;
    }

    public void voteDemocrat()
    {
        democratCount ++;
    }

    public void voteRepublican()
    {
        republicanCount ++;
    }

    public void showTallies()
    {
        System.out.println("Democrat: " + democratCount);
        System.out.println("Republica: " + republicanCount);
    }

}
