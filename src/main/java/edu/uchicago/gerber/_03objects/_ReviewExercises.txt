#########################################################################
# Use this file to answer Review Exercises from the Big Java textbook
#########################################################################

R8.1 Encapsulation

Encapsulation is defined as the wrapping up of data under a single unit.
It is the mechanism that binds together code and the data it manipulates.
It is a protective shield that prevents the data from being accessed by the code outside this shield.
And users don't need to worry about implementation of it.



R8.4 Public interface
Public interface of a class is public properties and public functions which are accessible to programmers.
Programmers only need to know its functions and signature and don't need to know how to implement it.

However, implementation includes detailed procedure of how functionality is achieved. And it is not provided in public interface.



R8.7 Instance versus static

To use instance methods, we must have an object of its class already created.
And each instance method will be initiated everytime an instance is created.
However, for static methods, we don't need creat an object firstly. It can be referenced by class name.
It will be initialized when it is called.



R8.8 Mutator and accessor

Accessor methods are methods which only visit the value of objects but don't change them.
But mutator methods will change the value of objects.



R8.9 Implicit parameter

Implicit parameter will not appear on parameter lists but can be accessed by methods.
Explicit parameter will always be declared. We will pass the value each time we call the method.



R8.10 Implicit parameter
An instance method can only have one implicit parameter.
A static method cannot have any implicit parameters.
An instance method can have any numbers of explicit parameters.



R8.12 Constructors
Java don't require a constructor when we create a new class
because each new class will automatically have a constructor with no parameter passed.
A class can have any numbers of constructors because constructor can have any combinations of values as arguments (include 0).
When there are more than 1 constructors, the one whose parameter list has the same type order, number with how you pass arguments will be chosen.


R8.16 Instance variables
Private instance variables can only be accessed by methods of the same class.
It also cannot be accessed by any specific objects but it can be visited and changed by public methods of objects.



R8.19 The this reference

‘This’ is a reference variable that refers to the current object. It's an implicit parameter of instance methods.
'This' is useful because we can use syntax "this.varableA" to directly find and distinguish that
we are referring the instance variable "variableA" of current object. It's very convenient way to access instance variables and methods.



R8.20 Zero, null, false, empty String
zero is int type and be used with operator + - and other mathematical operators.
null is a variable that pointing no values. But it can be pointed to other values.
false is boolean type and can be used with ==, ! operators.
empty string is a string type whose length is 0 and it can be used with string instance methods.






