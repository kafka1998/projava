package edu.uchicago.gerber._03objects.P8_7;

public class ComboLockTester {
    public static void main(String[] args) {

        //Test lock class with different passwords
        ComboLock lock = new ComboLock(5,6,7);
        System.out.println("Set a lock which has the password: 5 6 7");

        System.out.println("Try left 5, right 6, right 7");
        lock.turnLeft(5);
        lock.turnRight(6);
        lock.turnRight(7);

        if (lock.open())
            System.out.println("The lock is opened");
        else System.out.println("The lock is still closed");

        lock.reset();
        System.out.println("Try right 5, left 6, right 7");
        lock.turnRight(5);
        lock.turnLeft(6);
        lock.turnRight(7);


        if (lock.open())
            System.out.println("The lock is opened");
        else System.out.println("The lock is still closed");

        lock.reset();
        System.out.println("Try left 5, right 6, left 7 left 8");
        lock.turnLeft(5);
        lock.turnRight(6);
        lock.turnLeft(7);
        lock.turnLeft(8);

        if (lock.open())
            System.out.println("The lock is opened");
        else System.out.println("The lock is still closed");

    }
}
