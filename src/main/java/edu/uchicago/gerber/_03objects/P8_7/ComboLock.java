package edu.uchicago.gerber._03objects.P8_7;

public class ComboLock {
    private int secret1, secret2, secret3;
    private int comb1, comb2, comb3;
    private int tryCount = 1;
    private boolean orderIsRight; // To track whether the order is right

    public ComboLock(int sec1, int sec2, int sec3)
    {
        secret1 = sec1;
        secret2 = sec2;
        secret3 = sec3;
        orderIsRight = true;
        tryCount = 1;
    }

    public void reset()
    {
        tryCount = 1;
        orderIsRight = true;
    }

    public void turnLeft(int ticks)
    {
        if (tryCount == 2)
        {
            comb2 = ticks;
            tryCount += 1;
        }
        else
        {
            orderIsRight = false;
        }

    }

    public void turnRight(int ticks)
    {
        if (tryCount == 1)
        {
            tryCount ++;
            comb1 = ticks;
        }
        else if (tryCount == 3)
        {
            tryCount ++;
            comb3 = ticks;
        }
        else orderIsRight = false;

    }

    public boolean open()
    {
        if(comb1 == secret1 && comb2 == secret2 && comb3 == secret3 && orderIsRight)
            return true;
        else return false;
    }

}
