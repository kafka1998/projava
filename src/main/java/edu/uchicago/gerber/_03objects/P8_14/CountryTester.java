package edu.uchicago.gerber._03objects.P8_14;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Scanner;
import java.util.Collections;

public class CountryTester {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("How many countries you want to input: ");
        int num = in.nextInt();
        String cname;
        int population;
        double area;
        ArrayList<Country> countries = new ArrayList<Country>();
        int maxAreaIndex = 0;
        double maxArea = -1.0;
        int maxPopulIndex = 0;
        int maxPopul = -1;
        int maxDensIndex = 0;

        double maxDens = -1;
        for(int i = 0; i < num; i++)
        {
            System.out.print("Enter the name: ");
            cname = in.next();
            System.out.print("Enter the area: ");
            area =  in.nextDouble();
            System.out.print("Enter the population: ");
            population = in.nextInt();
            countries.add(new Country(cname, population, area));
            if (area > maxArea)
            {
                maxArea = area;
                maxAreaIndex = i;
            }
            if (population > maxPopul)
            {
                maxPopulIndex = i;
                maxPopul = population;
            }
            if (population/area > maxDens)
            {
                maxDensIndex = i;
                maxDens = population/area;
            }
        }

        System.out.println("The country with the largest area is " + countries.get(maxAreaIndex).getName());
        System.out.println("The country with the largest population is " + countries.get(maxPopulIndex).getName());
        System.out.println("The country with the largest population density is " +countries.get(maxDensIndex).getName());
    }
}
