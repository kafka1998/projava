package edu.uchicago.gerber._03objects.P8_14;

public class Country {
    private String name;
    private int population;
    private double area;

    public Country(String countryName, int populationNumber, double area)
    {
        name = countryName;
        population = populationNumber;
        this.area = area;
    }

    public double getArea()
    {
        return area;
    }

    public String getName()
    {
        return name;
    }

    public int getPopulation()
    {
        return population;
    }
}
