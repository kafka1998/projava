package edu.uchicago.gerber._03objects.P8_5;

public class SodaCan {
    private double height;
    private double radius;

    public SodaCan(double h, double r)
    {
        height = h;
        radius = r;
    }

    public double getSurfaceArea()
    {
        return Math.PI * radius * radius * 2 + 2 * radius * Math.PI * height;
    }

    public double getVolume()
    {
        return Math.PI * radius * radius * height;
    }


}
