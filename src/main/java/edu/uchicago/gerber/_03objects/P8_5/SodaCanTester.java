package edu.uchicago.gerber._03objects.P8_5;

public class SodaCanTester {
    public static void main(String[] args) {
        //Create a can with height 7.5 and radius 2.2
        //Then report the surface area and volume

        SodaCan newSoda = new SodaCan(7.5, 2.2);
        System.out.println("A new soda can (height: 7.5 radius 2.2) is created");
        System.out.println("It surface area is " + newSoda.getSurfaceArea() + " and its volume is " + newSoda.getVolume());
    }
}
