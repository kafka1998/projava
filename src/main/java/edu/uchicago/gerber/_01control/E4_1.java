package edu.uchicago.gerber._01control;

import java.util.Scanner;

public class E4_1 {
    public static void main(String[] args) {

        //Calculate the sum of all even numbers between 2 and 100 (inclusive).

        System.out.println("------Problem a------");

        int evenSum = 0;
        for (int i = 2; i <= 100; i = i + 2)
        {
            evenSum += i;
        }

        System.out.println("The sum of all even numbers between 2 and 100 is " + evenSum);

        //Calculate the sum of all squares between 1 and 100 (inclusive).
        System.out.println("------Problem b------");

        int sqrSum = 0;

        for(int i = 1; i * i <= 100; i++)
        {
            sqrSum += i * i;
        }

        System.out.println("The sum of all squares between 1 and 100 (inclusive) is " + sqrSum);


        //Calculate all powers of 2 from 20 up to 220.

        System.out.println("------Problem c------");

        for (int i = 0; i <= 20; i++)
        {
            System.out.println("2 to the power " + i + " is: " + Math.pow(2, i));
        }


        //Calculate the sum of all odd numbers between a and b (inclusive), where a and b are inputs.
        System.out.println("------Problem d------");
        Scanner in = new Scanner(System.in);

        System.out.print("Enter the lower bound: ");
        int lowBound = in.nextInt();
        System.out.print("Enter the upper bound: ");
        int upBound = in.nextInt();

        int temp = lowBound;
        int oddSum = 0;

        while (temp <= upBound)
        {
            if (temp % 2 == 1)
                oddSum +=  temp;
            temp += 1;
        }

        System.out.println("the sum of all odd numbers between " + lowBound + " and " + upBound + " is: " + oddSum);

        //Calculate the sum of all odd digits of an input
        System.out.println("------Problem e------");

        System.out.print("Enter the number:");
        in = new Scanner(System.in);
        String numString = in.nextLine();

        int oddDigitSum = 0;

        for(int i = 0; i < numString.length(); i++)
        {
            int digit = Integer.parseInt(numString.substring(i, i+1));

            if (digit % 2 == 1)
                oddDigitSum += digit;
        }

        System.out.println("The sum of all odd digits of " + numString + " is: " +oddDigitSum);
    }
}
