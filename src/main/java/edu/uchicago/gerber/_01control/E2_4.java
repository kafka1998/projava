package edu.uchicago.gerber._01control;

import java.util.Scanner;

public class E2_4 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        System.out.print("Enter the first number: ");
        int first = in.nextInt();

        System.out.print("Enter the second number: ");
        int second = in.nextInt();

        //Print related information
        System.out.println("The sum of " + first + " and " + second + " is " + (first + second));

        System.out.println("The difference of " + first + " and " + second + " is " + (first - second));

        System.out.println("The product of " + first + " and " + second + " is " + (first * second));

        System.out.println("The average of " + first + " and " + second + " is " + ((float)first + second)/2);

        System.out.println("The The distance (absolute value of the difference) of " + first + " and " + second + " is " + Math.abs(first - second));

        System.out.println("The maximum of " + first + " and " + second + " is " + Math.max(first,second));

        System.out.println("The minimum of " + first + " and " + second + " is " + Math.min(first,second));


    }


}
