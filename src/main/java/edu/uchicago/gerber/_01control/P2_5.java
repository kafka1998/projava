package edu.uchicago.gerber._01control;

import java.util.Scanner;

public class P2_5 {
    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);

        System.out.print("Enter the price: ");

        double price = in.nextDouble();

        //Calculate dollars and cents
        int dollar = (int) price;
        double difference = (price - dollar) * 100 + 0.5;
        int cents = (int) difference;

        System.out.println("Dollars: " + dollar);
        System.out.println("Cents: " + cents);



    }
}
