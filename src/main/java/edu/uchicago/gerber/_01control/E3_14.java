package edu.uchicago.gerber._01control;

import java.util.Scanner;

public class E3_14 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        System.out.print("Enter the month: ");
        int month = in.nextInt();

        System.out.print("Enter the day: ");
        int day = in.nextInt();

        String season;

        //assign seasons and adjust later
        if(month <= 3)
            season = "Winter";
        else if(month <= 6)
            season = "Spring";
        else if(month <= 9)
            season = "Summer";
        else if(month <= 12)
            season = "Fall";
        else
            season = "Error: Month should be equal or smaller than 12";

        //adjust seasons
        if(month % 3 == 0 && day >= 21) {

            if(season.equals("Winter"))
                season = "Spring";
            else if(season.equals("Spring"))
                season = "Summer";
            else if(season.equals("Summer"))
                season = "Fall";
            else
                season = "Winter";
        }


        System.out.println("Season: " + season);


    }
}
