package edu.uchicago.gerber._01control;

import java.util.Scanner;

public class E2_6 {
    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);

        System.out.print("Enter the meter number: ");


        double meters = in.nextDouble();

        //Calculate converted units
        double miles = meters * 0.000621371;
        double feet = meters * 3.28084;
        double inches = meters * 39.3701;

        //print information
        System.out.println("Converted miles: " + miles);
        System.out.println("Converted feets " + feet);
        System.out.println("Converted inches: " + inches);

    }
}
