package edu.uchicago.gerber._01control;

import java.util.Scanner;

public class E4_17 {
    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);

        System.out.print("Enter the number: ");

        int number = in.nextInt();

        while(number > 0)
        {
            System.out.println(number % 2);
            number = number / 2;
        }

    }
}
