package edu.uchicago.gerber._01control;

import java.util.Scanner;

public class P3_14 {
    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);

        System.out.print("Enter the year: ");

        int year = in.nextInt();

        if (((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0))
            System.out.print(year + " IS the leap year");
        else
            System.out.print(year + " IS NOT the leap year");

    }
}
