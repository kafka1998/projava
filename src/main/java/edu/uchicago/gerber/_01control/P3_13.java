package edu.uchicago.gerber._01control;

import java.util.Scanner;

public class P3_13 {
    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);

        System.out.print("Enter the number: ");

        int number = in.nextInt();
        int remainDigits = number;
        String romanNum = "";

        if(number > 3999){
            System.out.print("Invalid input");
            return;
        }

        int unitDigit = remainDigits % 10;
        remainDigits = remainDigits / 10;


        //calculate the unit digit in roman system
        switch (unitDigit)
        {
            case 0: break;
            case 1: romanNum = "I" + romanNum; break;
            case 2: romanNum = "II" + romanNum; break;
            case 3: romanNum = "III" + romanNum; break;
            case 4: romanNum = "IV" + romanNum; break;
            case 5: romanNum = "V" + romanNum; break;
            case 6: romanNum = "VI" + romanNum; break;
            case 7: romanNum = "VII" + romanNum; break;
            case 8: romanNum = "VIII" + romanNum; break;
            case 9: romanNum = "IX" + romanNum; break;
        }

        int tensDigit = remainDigits % 10;
        remainDigits = remainDigits / 10;

        //calculate the tens digit in roman system
        switch (tensDigit)
        {
            case 0: break;
            case 1: romanNum = "X" + romanNum; break;
            case 2: romanNum = "XX" + romanNum; break;
            case 3: romanNum = "XXX" + romanNum; break;
            case 4: romanNum = "XL" + romanNum; break;
            case 5: romanNum = "L" + romanNum; break;
            case 6: romanNum = "LX" + romanNum; break;
            case 7: romanNum = "LXX" + romanNum; break;
            case 8: romanNum = "LXXX" + romanNum; break;
            case 9: romanNum = "XC" + romanNum; break;
        }

        int hundredDigit = remainDigits % 10;
        remainDigits = remainDigits / 10;

        //calculate the hundred digit in roman system
        switch (hundredDigit)
        {
            case 0: break;
            case 1: romanNum = "C" + romanNum; break;
            case 2: romanNum = "CC" + romanNum; break;
            case 3: romanNum = "CCC" + romanNum; break;
            case 4: romanNum = "CD" + romanNum; break;
            case 5: romanNum = "D" + romanNum; break;
            case 6: romanNum = "DC" + romanNum; break;
            case 7: romanNum = "DCC" + romanNum; break;
            case 8: romanNum = "DCCC" + romanNum; break;
            case 9: romanNum = "CM" + romanNum; break;
        }

        int thousDigit = remainDigits % 10;
        remainDigits = remainDigits / 10;


        //calculate the thousands digit in roman system
        switch (thousDigit)
        {
            case 0: break;
            case 1: romanNum = "M" + romanNum; break;
            case 2: romanNum = "MM" + romanNum; break;
            case 3: romanNum = "MMM" + romanNum; break;
        }


        System.out.print("Converted Roman Number: " + romanNum);
    }
}
