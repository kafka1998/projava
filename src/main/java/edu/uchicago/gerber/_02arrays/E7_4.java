package edu.uchicago.gerber._02arrays;

import com.sun.tools.internal.ws.wsdl.document.soap.SOAPUse;

import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;
import java.io.File;


public class E7_4 {
    public static void main(String[] args) throws IOException
    {

        Scanner in = new Scanner(System.in);
        System.out.print("Enter the input file(there is a sample file existed called warAndPeace.txt): ");
        String fileName = in.next();
        System.out.print("Enter the output file: ");
        String output = in.next();

        Scanner reader = new Scanner(new File(fileName));
        FileWriter myWriter = new FileWriter(output);
        int i = 0;
        while (reader.hasNextLine())
        {
            String line = reader.nextLine();
            i++;
            myWriter.write("/* " + i + " */ " + line + "\n");
        }

        myWriter.close();
        reader.close();




    }
}
