package edu.uchicago.gerber._02arrays;
import java.util.*;

public class E6_12 {
    public static void main(String[] args)
    {
        int randomArray[] = new int[20];
        for (int i = 0; i < 20; i++)
        {
            randomArray[i] = (int) (Math.random() * 100);
        }
        System.out.println("The original array is: " + Arrays.toString(randomArray));

        //sort the array
        Arrays.sort(randomArray);

        System.out.println("Sorted array is " + Arrays.toString(randomArray));

    }


    }






