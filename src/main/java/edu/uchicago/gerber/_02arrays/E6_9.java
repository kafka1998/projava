package edu.uchicago.gerber._02arrays;
import java.util.*;

public class E6_9 {
    public static boolean equals(int[] a, int[] b)
    {
        if (a.length != b.length)
            return false;

        for (int i = 0; i < a.length; i++)
        {
            if (a[i] != b[i])
                return false;
        }
        return true;
    }

    public static void main(String[] args) {
        int[] arr1 = {1,2,3,4,4};
        int[] arr2 = {1,2,3,4,5};
        System.out.println(Arrays.toString(arr1));
        System.out.println(Arrays.toString(arr2));

        if (equals(arr1, arr2))
            System.out.println("They are same arrays");
        else
            System.out.println("They are not same arrays");
    }
}
