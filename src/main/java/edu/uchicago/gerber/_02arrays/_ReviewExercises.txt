#########################################################################
# Use this file to answer Review Exercises from the Big Java textbook
#########################################################################

R5.6 Parameters and return values.  The difference between an argument and a return value is that an argument is passed
into a method, whereas a return value is the value returned from it.

You can have n number of arguments, whereas you can only have one return value. By varying either the number and/or type of arguments,
you change the method's signature, which means you may overload a method. Varying the return value only does not change the
method's signature and therefore does not qualify the method for overloading.

Since Java5, you can even specify a variable number of arguments aka varargs which looks something like this:
 private String[] someMethod(int nGrade, String... strNickNames) {//implementation};
Notice that the varargs parameter "String... strNickNames" must be listed last.  Calling this method would look something like this:
 String[] strReturnValues = someMethod(99, "Jim", "Jamie", "James", "Jamey", "Jimmy", "Jimbo");



R5.8 Pseudocode

 Read the phone number store it in a new string

 for each character in the string
    if the character is not a number
        replace it with corresponding number

 return the converted string


R5.10 Variable scope

 Variable               Scope
 i(main),b(main)        main function, g function
 i(f),n(f)              f function
 a,b(g)                 g function
 n(g)                   for loop in g


 Output: 26



R5.14 Pass by value versus pass by reference

 when x and y are passed into falseSwap function, their copies instead of references are passed into function.
 Values of their copies are swapped inside the function, but their original values stay same.


 R6.3 Considering loops

 a) 25
 b) 13
 c) 12
 d) ArrayIndexOutOfBoundsException
 e) 11
 f) 25
 g) 12
 h) -1


 R6.10 Enhanced for-loop

 a) for(double i: values)
    {
        total = total + i;
    }


 b) int count = 0;
    for(double i: values)
    {
        if (count >= 1)
            total = total + i;
        count ++;
    }

 c) int count = 0;
    for(double i: values)
    {
        if (i == target)
            return count;
        count ++;
    }


 R6.23 Computing runs
    initiate maxrun as 0 to store the longest run we have found
    initiate i as 0 to track the index of traversal
    while i < values.length
        initiate the curentRun as 1 to count the length of current run
        while i + 1 < values.length
            i ++
            if values[i] == values[i-1]
                currentRun ++
            else
                break the loop to start another run
        if currentRun > maxrun
            maxrun = currentRun
     return maxrun


 R6.29 Multi-dimensional arrays

 a) for(int i = 0; i < ROWS; i++)
    {
        for(int j = 0; j < COLUMNS; j++)
            values[i][j] = 0;
    }


 b) for(int i = 0; i < ROWS; i++)
    {
        for(int j = 0; j < COLUMNS; j++)
        {
            if ((i + j)%2 == 0)
                values[i][j] = 0;
            else
                values[i][j] = 1;
        }
    }

 c) for(int i = 0; i < ROWS; i++)
    {
        for(int j = 0; j < COLUMNS; j++)
        {
            if (i == 0 || i == ROWS -1)
                values[i][j] = 0;
        }
    }


 d) int sum = 0;
    for(int i = 0; i < ROWS; i++)
    {
        for(int j = 0; j < COLUMNS; j++)
            sum = sum + values[i][j];
    }


 e) for(int i = 0; i < ROWS; i++)
    {
        for(int j = 0; j < COLUMNS; j++)
            System.out.print(values[i][j] + " ")
        System.out.println()
    }

 R6.34 Understanding arrays

 a) True
 b) True
 c) False
 d) True
 e) False
 f) False


R7.1 Exceptions
If you try to open a file for reading that doesn’t exist, a exception called FileNotFoundException will be thrown
If you try to open a file for writing that doesn’t exist, the program will create the file with the same name.


R7.6 Throwing and catching

When you throw an exception, you want to stop the execution of current codes and go the execution of exception handler.
For catching the exception, some exceptions are thrown and exception handler is called.
Catching the exception is to handle the thrown exception.


R7.7 Checked versus unchecked

Checked exceptions are the exceptions that are checked at compile time.
It is a type of exception that must be either caught or declared in the method in which it is thrown.
Example: java.io.IOException is a checked exception.

Unchecked are the exceptions that are not checked at compiled time.
It reflects some error inside the program logic.
Example: ArithmeticException is an unchecked exception.



R7.8 Exceptions philosophy in Java

IndexOutOfBoundsException is an unchecked exception and will be thrown at runtime.
It is thrown because the program want to access the array index that doesn't exist.
It shows that there is logical error in the program, and programmer needs to know it and modify.
Therefore, we don't need to declare that.


R7.11 What is an exception object

Exception object contains information related.
We can print out the chain of method calls that cause the exception by calling exception.printStackTrace().
Also, we can call exception.getMessage() to get the deatailed message of the exception.


R7.15 Scanner exceptions. Explain why these are either checked or unchecked.

next() can throw NoSuchElementException when you try to read input from scanner and when input does not exists.
NoSuchElementException is an unchecked exception.

nextInt() can throw InputMismatchException when use entered the non-integer input.
InputMismatchException is an unchecked exception.







