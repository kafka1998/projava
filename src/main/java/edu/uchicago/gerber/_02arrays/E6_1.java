package edu.uchicago.gerber._02arrays;

public class E6_1 {

    public static void print_even_index(int[] array) {
        System.out.print("The even indexes of array are: ");
        for (int i = 0; i < array.length; i++) {
            if (i % 2 == 0)
                System.out.print(array[i] + " ");
        }
        System.out.println();
    }

    public static void print_even_elem(int[] array) {
        System.out.print("The even elements of array are: ");
        for (int i = 0; i < array.length; i++) {
            if (array[i] % 2 == 0)
                System.out.print(array[i] + " ");
        }
        System.out.println();
    }

    public static void print_reverse_order(int[] array) {
        System.out.print("The reverse order of array are: ");
        for (int i = array.length - 1; i >= 0; i--) {
            System.out.print(array[i] + " ");
        }
        System.out.println();
    }

    public static void print_first_last_elem(int[] array) {
        System.out.println("The first element of array is " + array[0]);
        System.out.println("The last element of array is " + array[array.length - 1]);
    }

    public static void main(String[] args) {
        int randomArray[] = new int[10];
        System.out.print("The original array is: ");
        for (int i = 0; i < 10; i++)
        {
            randomArray[i] = (int) (Math.random() * 1000 + 1);
            System.out.print(randomArray[i] + " ");
        }
        System.out.println();

        print_even_index(randomArray);
        print_even_elem(randomArray);
        print_reverse_order(randomArray);
        print_first_last_elem(randomArray);

    }
}
