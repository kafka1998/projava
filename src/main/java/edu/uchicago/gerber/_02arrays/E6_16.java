package edu.uchicago.gerber._02arrays;

import java.util.Scanner;

public class E6_16 {

    public static void main(String[] args) {
        System.out.print("Enter the number of bars: ");
        Scanner in = new Scanner(System.in);
        int barNnum = in.nextInt();

        int bars[] = new int[barNnum];
        int max = -999;
        for (int i = 0; i < barNnum; i++)
        {
            System.out.print("Enter the number " + i + "(under 20):");
            bars[i] = in.nextInt();
            if (bars[i] > max)
                max = bars[i];
        }


        //print bars
        for (int i = max; i > 0; i--)
        {
            for (int index = 0; index < barNnum; index++)
            {
                if (bars[index] >= i)
                    System.out.print("*");
                else
                    System.out.print(" ");
            }
            System.out.println();
        }


    }
}
