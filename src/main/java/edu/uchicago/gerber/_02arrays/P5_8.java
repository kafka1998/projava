package edu.uchicago.gerber._02arrays;

import com.sun.javaws.IconUtil;

import java.util.Scanner;

public class P5_8 {

    //Tests whether a year is a leap year
    //Use multiple if statements and return statements
    //To return the result as soon as you know it
    public static boolean isLeapYear(int year)
    {
        if (year % 400 == 0)
            return true;
        else if (year % 100 == 0)
            return false;
        else  if (year % 4 == 0)
            return true;
        else return false;

    }

    //input the date and output the info
    public static void main(String[] args)
    {
        System.out.print("Enter the year:");
        Scanner in = new Scanner(System.in);
        int year = in.nextInt();
        if (isLeapYear(year))
            System.out.println(year + " is a leap year");
        else
            System.out.println(year + " is not a leap year");
    }
}
