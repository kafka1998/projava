package edu.uchicago.gerber._02arrays;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import java.util.Scanner;

public class P7_5 {
    private static List<String[]> content;
    public static int numberOfRows() throws Exception
    {
        return content.size();
    }

    public static int numberOfFields(int row) throws Exception
    {

        return content.get(row - 1).length;
    }

    public static String field(int row, int column) throws Exception
    {
        return content.get(row - 1)[column - 1];
    }

    public static void main(String[] args) throws Exception {

        //load csv file
        Scanner in = new Scanner(System.in);
        System.out.print("Enter the input file(there is a sample file existed called input.csv): ");
        String filename = in.next();
        Scanner reader = new Scanner(new File(filename));
        content = new ArrayList<>();
        while (reader.hasNextLine())
        {
            String line = reader.nextLine();
            content.add(line.split(","));
        }
        reader.close();

        //Test member functions
        System.out.println("Test each function\n");
        System.out.println("Total number of rows is: " + numberOfRows());
        System.out.print("Enter a row number(1 based): ");
        in = new Scanner(System.in);
        int rowNum = in.nextInt();
        System.out.println("The number of fileds is " + numberOfFields(rowNum));

        System.out.print("Enter a row number(1 based): ");
        int row = in.nextInt();
        System.out.print("Enter a col number(1 based): ");
        int col = in.nextInt();

        System.out.println("The filed is " + field(row,col));
    }
}
