package edu.uchicago.gerber._02arrays;

import java.util.Scanner;

public class P5_24 {

    //convert single roman letter to decimal number
    public static int letter_to_num(char letter)
    {
        if (letter == 'I')
            return 1;
        else if (letter == 'V')
            return 5;
        else if (letter == 'X')
            return 10;
        else if (letter == 'L')
            return 50;
        else if (letter == 'C')
            return 100;
        else if (letter == 'D')
            return 500;
        else if (letter == 'M')
            return 1000;
        else return 0;
    }

    //convert the roman number string to the decimal number
    public static int roman_to_deci(String roman)
    {
        int total = 0;
        String temp = roman;

        while (temp.length() != 0)
        {
            if (temp.length() == 1)
            {
                total += letter_to_num(temp.charAt(0));
                temp = "";
            }
            else if (letter_to_num(temp.charAt(0)) >= letter_to_num(temp.charAt(1)))
            {
                total += letter_to_num(temp.charAt(0));
                temp = temp.substring(1);
            }
            else
            {
                int diff = letter_to_num(temp.charAt(1)) - letter_to_num(temp.charAt(0));
                total += diff;
                if (temp.length() == 2)
                    temp = "";
                else temp = temp.substring(2);
            }
        }

        return total;
    }

    //input the roman number and output the decimal number
    public static void main(String[] args)
    {
        System.out.print("Enter the roman number: ");
        Scanner in = new Scanner(System.in);
        String roman = in.next();
        int num = roman_to_deci(roman);
        System.out.println("The decimal number of " + roman + " is " + num);
    }
}
