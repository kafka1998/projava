package edu.uchicago.gerber._08final.mvc.model;

import java.awt.*;
import java.util.Arrays;

public class Debris extends Sprite {

    private final int RAD = 2;
    private final int MAX_EXPIRE = 20;

    public Debris(Movable astExploded){
        super();
        setTeam(Team.DEBRIS);

        setExpire(MAX_EXPIRE);
        //the spin will be either plus or minus 0-9
        int nSpin = edu.uchicago.gerber._08final.mvc.controller.Game.R.nextInt(10);
        if(nSpin % 2 ==0)
            nSpin = -nSpin;

        //random delta-x
        int nDX = edu.uchicago.gerber._08final.mvc.controller.Game.R.nextInt(10);
        if(nDX %2 ==0)
            nDX = -nDX;
        setDeltaX(nDX);

        //random delta-y
        int nDY = edu.uchicago.gerber._08final.mvc.controller.Game.R.nextInt(10);
        if(nDY %2 ==0)
            nDY = -nDY;
        setDeltaY(nDY);

        setRadius(RAD);

        assignRandomShape();

        setCenter(astExploded.getCenter());

    }

    public void assignRandomShape ()
    {
        setColor(Color.red);

        int nSide = edu.uchicago.gerber._08final.mvc.controller.Game.R.nextInt( 7 ) + 7;
        int nSidesTemp = nSide;

        int[] nSides = new int[nSide];
        for ( int nC = 0; nC < nSides.length; nC++ )
        {
            int n = nC * 48 / nSides.length - 4 + edu.uchicago.gerber._08final.mvc.controller.Game.R.nextInt( 8 );
            if ( n >= 48 || n < 0 )
            {
                n = 0;
                nSidesTemp--;
            }
            nSides[nC] = n;
        }

        Arrays.sort( nSides );

        double[]  dDegrees = new double[nSidesTemp];
        for ( int nC = 0; nC <dDegrees.length; nC++ )
        {
            dDegrees[nC] = nSides[nC] * Math.PI / 24 + Math.PI / 2;
        }
        setDegrees( dDegrees);

        double[] dLengths = new double[dDegrees.length];
        for (int nC = 0; nC < dDegrees.length; nC++) {
            if(nC %3 == 0)
                dLengths[nC] = 1 - edu.uchicago.gerber._08final.mvc.controller.Game.R.nextInt(40)/100.0;
            else
                dLengths[nC] = 1;
        }
        setLengths(dLengths);
    }

    private int adjustColor(int nCol, int nAdj) {
        if (nCol - nAdj <= 0) {
            return 0;
        } else {
            return nCol - nAdj;
        }
    }
    @Override
    public void move() {
        super.move();
        setFadeValue(getExpire() * 255 / MAX_EXPIRE);
        Color colDebris = new Color(getFadeValue(), 0, 0);
        setColor(colDebris);

        if (getExpire() < MAX_EXPIRE ){
            setDeltaX(getDeltaX() * 1.07);
            setDeltaY(getDeltaY() * 1.07);
        }

        if (getExpire() == 0)
            CommandCenter.getInstance().getOpsList().enqueue(this, CollisionOp.Operation.REMOVE);
        else
            setExpire(getExpire() - 1);
    }
}
