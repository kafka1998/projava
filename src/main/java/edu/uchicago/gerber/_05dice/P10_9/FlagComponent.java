package edu.uchicago.gerber._05dice.P10_9;

import sun.print.ProxyGraphics;

import javax.swing.*;
import java.awt.*;

public class FlagComponent extends JComponent {
    private  int xLeft;
    private int yTop;
    private  int width;
    private  int height;
    private  Color color1;
    private Color color2;
    private Color color3;


    FlagComponent(int xLeft,int yTop,int width,int height,Color color1,Color color2,Color color3){
        this.xLeft=xLeft;
        this.yTop=yTop;
        this.width=width;
        this.height=height;
        this.color1=color1;
        this.color2=color2;
        this.color3=color3;

    }



    public void paint(Graphics g) {

        g.setColor(color1);
        g.fillRect(xLeft,yTop,width,height/3);
        g.setColor(color2);
        g.fillRect(xLeft,yTop+height/3,width,height/3);
        g.setColor(color3);
        g.fillRect(xLeft,yTop+2*height/3,width,height/3);

    }




}
