package edu.uchicago.gerber._05dice.P11_9;
import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class GUI extends JFrame {
    private int x, y,oldx,oldy;
    boolean ifclick=false;
    public GUI () {
        addHandler();
        setSize(500, 500);
        setLocation(350, 150);
        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
    public static void main(String[] args) {
        new GUI ();
    }
    private void addHandler() {
        addMouseListener(new MouseAdapter() {

            public void mouseClicked(MouseEvent e) {
                if (ifclick) {

                    ifclick=!ifclick;

                    repaint();
                } else {
                    oldx=e.getX();
                    oldy=e.getY();
                    x = e.getX()-50;
                    y = e.getY()-50;

                    ifclick=!ifclick;
                    repaint();
                }
            }
        });
    }
    public void paint(Graphics g) {
        super.paint(g);
        if(!ifclick){
            g.setColor(Color.red);
            g.drawOval(oldx,oldy, 100, 100);
        }else {
            g.setColor(Color.red);
            g.drawOval(x,y,100,100);
        }
    }
}
