package edu.uchicago.gerber._05dice.P10_19;


import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;

public class Restaurant {
    Restaurant(){
        JFrame jFrame = new JFrame();
        Font font = new Font("",Font.PLAIN,30);

        JPanel jPanel = new JPanel();
        jPanel.setSize(1000,500);
        LayoutManager layoutManager = new GridLayout(12,2,10,10);

        JTextField []jTextField=new JTextField[10];
        for (int i=0;i<jTextField.length;i++){
            jTextField[i]=new JTextField();
            jTextField[i].setFont(font);
        }
        JLabel [] menu = new JLabel[10];
        String [] name = {"MapoTofu 10","Boiled Fish 20","Twice Cooked Pork 30","Sticker 40","ChickenLegs 50"," Chicken Salad 60"
                ,"Fried Prawns 70","  Fried Oysters 80","  Fried Squid 90","Smoked Jelly Fish 100"
        };
        for(int i = 0;i<menu.length;i++){
            menu[i]=new JLabel(name[i]);

            menu[i].setFont(font);
            jPanel.add(menu[i]);
            jPanel.add(jTextField[i]);
        }


        JButton yes= new JButton("yes");
        yes.setFont(font);
        JButton clear = new JButton("clear");
        clear.setFont(font);
        jPanel.add(yes);
        JTextArea result= new JTextArea();

        yes.addActionListener(e -> {

            int []number =new int[10];
            int sum=0;
            for(int i =0;i<jTextField.length;i++){
                number[i]= Integer.parseInt(jTextField[i].getText());
                sum+=number[i]*(i+1)*10;
            }
            result.setText("all food:"+sum+"\n");
            result.append("taxes: "+sum*0.1+"\n");
            result.append("tip: "+sum*0.1+"\n");
            result.append("all: "+(sum+sum*0.2));

        });
        clear.addActionListener(e->{
            result.setText("");
        });

        jPanel.add(clear);

        jPanel.setLayout(layoutManager);
        jFrame.add(jPanel);

        result.setFont(font);
        result.setBounds(0,500,1000,600);
        jFrame.add(result);
        jFrame.setSize(1000,800);
        jFrame.setLayout(null);




        jFrame.setVisible(true);





    }





}
