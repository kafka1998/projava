package edu.uchicago.gerber._05dice.pig;


import java.awt.*;
import javax.swing.*;

public class PigGUI extends JFrame {
    //定义属性
    private JFrame frame = new JFrame();
    private JPanel panel = new JPanel();
    private JTextField text = new JTextField(10);
    private JButton start = new JButton("Start");
    private JButton getsorce=new JButton("getsorce");

    private JButton stop = new JButton("stop");
    private JLabel you= new JLabel("your sorce:");
    private JLabel yoursorce = new JLabel("");
    private  int sorce =0;
    private JLabel computer = new JLabel("computersorce:");
    private JLabel cs=new JLabel();

    private Boolean Flag=false;
    private String buf=new String();
    private  int computersorce=0;
    private int allcomputersorce=0;

    private void AppearanceSetting(){
        frame.setSize(300, 100);
        frame.setLayout(new FlowLayout());
    }


    private void FrameSetting(){
        Font font = new Font("",Font.PLAIN,20);
        text.setFont(font);
        start.setFont(font);
        stop.setFont(font);
        you.setFont(font);
        getsorce.setFont(font);
        panel.setFont(font);
        computer.setFont(font);
        cs.setFont(font);
        text.setEditable(false);
        panel.add(text);
        panel.add(start);
        panel.add(stop);
        panel.add(you);

        panel.add(getsorce);
        getsorce.addActionListener(e -> {
            Flag=false;
            if (text.getText().equals("1")){
                sorce=0;
                getsorce.setText("0");
                computersorce=0;
                new Thread(() -> {
                    while(true){
                        if(computersorce<21){
                            try {
                                Thread.sleep(100);
                            } catch (InterruptedException e1) {
                                e1.printStackTrace();
                            }

                            SwingUtilities.invokeLater(() -> {
                                // TODO Auto-generated method stub

                                text.setText(String.valueOf((int)((Math.random()*6)+1)));
                                computersorce+=Integer.parseInt(text.getText());
                                allcomputersorce+=Integer.parseInt(text.getText());
                                cs.setText(String.valueOf(allcomputersorce));

                            });

                        }
                    }

                }).start();
            }else {
                sorce+=Integer.parseInt(text.getText());
                getsorce.setText(String.valueOf(sorce));
            }

        });
        panel.add(yoursorce);
        panel.add(computer);
        panel.add(cs);
        panel.setLayout(new FlowLayout());
        frame.add(panel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }


    private void Jb1Listener(){

        start.addActionListener(e -> {

            Flag=true;

            new Thread(() -> {
                while(true){


                    if(Flag){
                        try {
                            Thread.sleep(100);
                        } catch (InterruptedException e1) {
                            e1.printStackTrace();
                        }

                        SwingUtilities.invokeLater(() -> {
                            // TODO Auto-generated method stub

                            text.setText(String.valueOf((int)((Math.random()*6)+1)));


                        });

                    }
                }

            }).start();
        });
    }



    private void Jb2Listener(){
        stop.addActionListener(e -> Flag=false);
    }



    public void Demo(){
        AppearanceSetting();
        FrameSetting();
        Jb1Listener();
        Jb2Listener();
    }

    public static void main(String[] args) {

        SwingUtilities.invokeLater(() -> new PigGUI().Demo());


    }
}

