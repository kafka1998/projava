package edu.uchicago.gerber._05dice.P10_10;

import javax.swing.*;
import java.awt.*;

import java.applet.Applet;

import java.awt.Color;

import java.awt.Font;

public class Rings extends Applet

{

    private int r=50,x=120,y=80,d=100,j=151,k=160;
    public void paint(Graphics g)

    {
        Font font = new Font("",Font.PLAIN,20);

        Graphics2D g0=(Graphics2D)g;
        BasicStroke  bs=new BasicStroke(10);
        g0.setStroke(bs);
        g0.setColor(Color.BLUE);

        g0.drawOval(x-r,y-r,d,d);

        g0.setColor(Color.black);

        g0.drawOval((x+62)-r,y-r,d,d);

        g0.setColor(Color.red);

        g0.drawOval((x+114)-r,y-r,d,d);

        g0.setColor(Color.yellow);

        g0.drawOval(j-r,k-r,d,d);

        g0.setColor(Color.green);

        g0.drawOval((j+62)-r,k-r,d,d);

        g0.setColor(Color.blue);

        g0.setFont(font);

        g0.drawString("Welcome to BeiJing",90,269);

    }//END PAINT

    public static void main(String[] args) {
        Rings gui = new Rings();
        JFrame jFrame = new JFrame();
        jFrame.getContentPane().add(gui, BorderLayout.CENTER);
        jFrame.setSize(600,500);
        jFrame.setVisible(true);
    }

}//END CLASS
