package edu.uchicago.gerber._04interfaces.P9_6;

public class Monthly extends Appointment{
    public Monthly(String desc, int year, int month, int day)
    {
        super(desc, year, month, day);
    }

    @Override
    public boolean occursOn(int year, int month, int day)
    {
        if (this.getYear() == year)
        {
            if (this.getMonth() <= month)
            {
                if (this.getDay() == day)
                    return true;
            }
        }

        if (this.getYear() < year)
        {
            if (this.getDay() == day)
                return true;
        }

        return false;
    }
}
