package edu.uchicago.gerber._04interfaces.P9_6;

import java.util.Scanner;

public class AppointmentTest {
    public static void main(String[] args) {
        Appointment[] appointments = new Appointment[4];
        appointments[0] = new Onetime("Java Meeting", 2021, 10,3);
        appointments[1] = new Daily("Dental", 2020, 10,10);
        appointments[2] = new Monthly("Novel Meeting", 2018, 9,9);
        appointments[3] = new Monthly("Boxing", 2020,10,9);
        Scanner in = new Scanner(System.in);
        System.out.print("Enter the year: ");
        int year = in.nextInt();
        System.out.print("Enter the month: ");
        int month = in.nextInt();
        System.out.print("Enter the day: ");
        int day = in.nextInt();
        for (Appointment a : appointments)
        {
            if (a.occursOn(year,month,day))
                System.out.println(a.getDescription());
        }

    }
}
