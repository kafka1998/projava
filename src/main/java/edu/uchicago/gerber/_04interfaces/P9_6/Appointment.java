package edu.uchicago.gerber._04interfaces.P9_6;

public class Appointment {
    private String description;
    private int year;
    private int month;
    private int day;

    public Appointment(String desc, int year, int month, int day)
    {
        this.description = desc;
        this.year = year;
        this.month = month;
        this.day = day;
    }

    public boolean occursOn(int year, int month, int day)
    {
        if (year == this.year && month == this.month && day == this.day)
            return true;
        else return false;
    }

    public String getDescription()
    {
        return this.description;
    }

    public int getDay()
    {
        return day;
    }

    public int getYear()
    {
        return year;
    }

    public int getMonth()
    {
        return month;
    }
}
