package edu.uchicago.gerber._04interfaces.P9_6;

public class Onetime extends Appointment{
    public Onetime(String desc, int year, int month, int day)
    {
        super(desc, year, month, day);
    }
}
