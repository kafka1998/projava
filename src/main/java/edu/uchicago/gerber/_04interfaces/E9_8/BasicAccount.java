package edu.uchicago.gerber._04interfaces.E9_8;

public class BasicAccount extends BankAccount{
    @Override
    public void withdraw(double amount) {
        if (amount <= this.getBalance())
            super.withdraw(amount);
        else System.out.println("There isn't enough money to withdraw!");
    }
}
