package edu.uchicago.gerber._04interfaces.E9_8;

public class BasicAccountTest {
    public static void main(String[] args) {
        BankAccount account = new BasicAccount();
        account.deposit(100);
        System.out.println("Create a account with 100 balance");
        System.out.println("Try to withdraw 101.......");
        account.withdraw(101);
        account.withdraw(70);
        System.out.println("After withdraw 70, there is " + account.getBalance() + " left in balance.");
    }
}
