#########################################################################
# Use this file to answer Review Exercises from the Big Java textbook
#########################################################################


R9.1 Superclas and subclass

        Superclass             Subclass
a.       Employee                Manager
b.       Student                 GraduateStudent
c.       Person                  Student
d.       Employee                Professor
e.       BankAccount             CheckingAccount
f.       Vehicle                 Car
g.       Vehicle                 Minivan
h.       Car                     Minivan
i.       Vehicle                 Truck



R9.2 superclass and subclass
 Toaster, CarVacuum, TravelIron behave very differently than each other
 and don't have enough generalization to be subclasses of SmallAppliance.



R9.4 SavingsAccount

inherited methods: deposit(), getBalance()
override  methods: withdraw(), monthEnd()
  added   methods: setInterestRate()



R9.6 Sandwich

a. x = y;                   legal
b. y = x;                   illegal
c. y = new Sandwich();      illegal
d. x = new Sub();           legal



R9.7 Inheretence -use the UML diagram tool in IntelliJ and indicate to the grader where uml file is located
uml path : _04interfaces/UML/R9_7.uml



R9.8 Inheretence -use the UML diagram tool in IntelliJ and indicate to the grader where uml file is located
uml path : _04interfaces/UML/R9_8.uml
I assume that car, cycle, motorcycle are considered as not absolutely included in sportUtilityVehicle



R9.9 Inheretence -use the UML diagram tool in IntelliJ and indicate to the grader where uml file is located
uml path : _04interfaces/UML/R9_3.uml
I assume that seminar is a form of lecture



R9.10 Casting

For (int) x, casting will change the primitive value since the primitive variable contains its value.
But for (BankAccount) x, the reference variable only refers to an object but doesn’t contain the object itself
cast here is casting the variable type of the object without changing its value, which narrows or extends methods of it.



R9.11 instanceof operator
a. True
b. True
c. False
d. True
e. False
f. False



R9.14 Edible interface

a. Legal
b. Illegal
c. Legal
d. Illegal
e. Illegal
f. Legal
g. Illegal
h. Illegal

