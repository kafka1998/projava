package edu.uchicago.gerber._04interfaces.E9_13;

import java.awt.*;

public class BetterRectangle extends Rectangle {
    public BetterRectangle(int x, int y, int width, int height)
    {
        setLocation(x,y);
        setSize(width, height);
    }

    public double getPerimeter()
    {
        return (getHeight() + getWidth()) * 2;
    }

    public double getArea()
    {
        return getHeight() * getWidth();
    }

}
