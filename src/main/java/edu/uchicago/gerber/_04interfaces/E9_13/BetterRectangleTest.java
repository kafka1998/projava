package edu.uchicago.gerber._04interfaces.E9_13;

public class BetterRectangleTest {
    public static void main(String[] args) {
        BetterRectangle rec = new BetterRectangle(10, 10, 8, 7);
        System.out.println("The area is: " + rec.getArea());
        System.out.println("The perimeter is: " + rec.getPerimeter());
    }
}
