package edu.uchicago.gerber._04interfaces.p9_1;

public class WorldClockTest {
    public static void main(String[] args) {
        Clock c1 = new Clock();
        System.out.println("Current time is " + c1.getTime());
        Clock c2 = new WorldClock(3);
        System.out.println("Time of three time zones ahead is " + c2.getTime());
        Clock c3 = new WorldClock(-3);
        System.out.println("Time of three time zones behind is " + c3.getTime());
    }
}
