package edu.uchicago.gerber._04interfaces.p9_1;

public class WorldClock extends Clock{
    private int offset;

    public WorldClock(int offset)
    {
        this.offset = offset;
    }

    @Override
    public String getHours() {
        return  java.time.LocalTime.now().plusHours(offset).toString().split(":")[0];
    }
}
