package edu.uchicago.gerber._04interfaces.p9_1;

public class Clock {

    public String getHours()
    {
        return  java.time.LocalTime.now().toString().split(":")[0];
    }

    public String getMinutes()
    {
        return  java.time.LocalTime.now().toString().split(":")[1];
    }

    public String getTime()
    {
        return getHours() + ":" + getMinutes();
    }
}
