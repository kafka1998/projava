package edu.uchicago.gerber._04interfaces.E9_17;

public class SodaCanTest {
    public static void main(String[] args) {
        SodaCan s1 = new SodaCan(2,2);
        SodaCan s2 = new SodaCan(2.5,2.5);
        SodaCan s3 = new SodaCan(3,3);
        SodaCan s4 = new SodaCan(4,4);
        SodaCan[] canArray = {s1, s2, s3, s4};

        int count = 0;
        double total = 0.0;

        for(SodaCan s : canArray)
        {
            total += s.measure();
            count ++;
        }

        System.out.println("The average area of the array of soda cans is: " + total/count);

    }
}
