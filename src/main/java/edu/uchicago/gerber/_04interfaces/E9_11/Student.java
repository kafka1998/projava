package edu.uchicago.gerber._04interfaces.E9_11;

public class Student extends Person{
    private String major;

    public Student(String name, int year, String major)
    {
        super(name, year);
        this.major = major;
    }

    @Override
    public String toString() {
        return super.toString() + "  Major: " + this.major;
    }
}
