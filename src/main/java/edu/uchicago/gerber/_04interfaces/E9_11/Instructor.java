package edu.uchicago.gerber._04interfaces.E9_11;

public class Instructor extends Person{
    private double salary;

    public Instructor(String name, int year, double salary)
    {
        super(name, year);
        this.salary = salary;
    }

    @Override
    public String toString() {
        return super.toString() + "  Salary: " + salary;
    }
}
