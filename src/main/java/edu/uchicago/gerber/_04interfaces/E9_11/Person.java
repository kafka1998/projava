package edu.uchicago.gerber._04interfaces.E9_11;

public class Person {
    private String name;
    private int birthYear;

    public Person(String name, int year)
    {
        this.name = name;
        this.birthYear = year;
    }

    public String toString()
    {
        return "Name: " + name + "  Year of Birth: " + birthYear;
    }
}
