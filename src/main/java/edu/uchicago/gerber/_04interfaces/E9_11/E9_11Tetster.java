package edu.uchicago.gerber._04interfaces.E9_11;

public class E9_11Tetster {
    public static void main(String[] args) {
        Person p1 = new Person("Kafka", 1998);
        Person p2 = new Student("Bolano", 2666, "computer science");
        Person p3 = new Instructor("Cat", 1960, 200000);
        System.out.println(p1);
        System.out.println(p2);
        System.out.println(p3);
    }
}
