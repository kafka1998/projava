TEXT FLY WITHIN
THE BOOK ONLY



Tight Binding Book



CO > UJ

ft <OU_1 68052 >m



OUP 2273 19-1 1-79 10,000 Copies.

OSMANIA UNIVERSITY LIBRARY

Call No ^^L Accession No ^95 gg

Author -j- 4.STt> y^

Title W<U *'<P4>*' ( *~'

This book should bc^rcturned on or before the date last marked below.



War and Peace



BY LEO TOLSTOY



Translated b\ LOUISE and AYLMER MAUDE




WILLIAM BENTON, Publisher



ENCYCLOPEDIA BR1TANNICA, INC.



CHICAGO - LONDON - TORONTO



BY ARRANGEMENT WITH OXFORD UNIVERSITY PRESS



COPYRIGHT IN THE UNITED STATES OF AMERICA, 1952,
BY ENCYCLOPEDIA BRITANNICA,INC.

COPYRIGHT 1952. COPYRIGHT UNDER INTERNATIONAL COPYRIG^
ENCYCLOP *:DIA BRITANNICA, INC. ALL RIGHTS RESERVED UNDER
COPYRIGHT CONVENTIONS BY ENCYCLOPEDIA BRITANJ^



BIOGRAPHICAL NOTE
LEO TOLSTOY, 18281910



COUNT LEO NIKOLAYEVICH TOLSTOY was born
August 28, 1828, at the family estate of Yasna-
ya Polyana, in the province of Tula. His moth-
er died when he was three and his father six
years later. Placed in the care of his aunts, he
passed many of his early years at Kazan, where,
in 1844, after a preliminary training by French
tutors, he entered the university. He cared lit-
tle for the university and in 1847 withdrew be-
cause of "ill-health and domestic circum-
stances." He had, however, done a great deal
of reading, of French, English, and Russian
novels, the New Testament, Voltaire, and
Hegel. The author exercising the greatest in-
fluence upon him at this time was Rousseau;
he read his complete works and for sometime
wore about his neck a medallion of Rousseau.

Immediately upon leaving the university,
Tolstoy returned to his estate and, perhaps inr
spired by his enthusiasm for Rousseau, pre-
pared to devote himself to agriculture and to
improving the condition of his serfs. His first
attempt at social reform proved disappointing,
and after six months he withdrew to Moscow
and St. Petersburg, where he gave himself over
to the irregular life characteristic of his class
and time. In 1851, determined to "escape my
debts and, more than anything else, my hab-
its," he enlisted in the Army as a gentleman-
volunteer, and went to the Caucasus. While at
Tiflis, preparing for his examinations as a
cadet, he wrote the first portion of the trilogy,
Childhood, Boyhood, and Youth, in which he
celebrated the happiness of "being with Na-
ture, seeing her, communing with her." He al-
so began The Cossacks with the intention of
showing that culture is the enemy of happi-
ness. Although continuing his army life, he
gradually came to realize that "a military ca-
reer is not for me, and the sooner I get out of
it and devote myself entirely to literature the
better." His Sevastopol Sketches (1855) were
so successful that Czar Nicholas issued special
orders that he should be removed from a post
of danger.

Returning to St. Petersburg, Tolstoy was re-
ceived with great favor in both the official and
literary circles of the capital. He soon became



interested in the popular progressive move-
ment of the time, and in 1857 he decided to go
abroad and study the educational and munici-
pal systems of other countries. That year, and
again in 1860, he traveled in Europe. At Yas-
naya Polyana in 1861 he liberated his serfs and
opened a school, established on the principle
that "everything which savours of compulsion
is harmful." He started a magazine to promote
his notions on education and at the same time
served as an official arbitrator for grievances
between the nobles and the recently emanci-
pated serfs. By the end of 1863 he was so ex-
hausted that he discontinued his activities and
retired to the steppes to drink koumis for his
health.

Tolstoy had been contemplating marriage
for some time, and in 1862 he married Sophie
Behrs, sixteen years his junior, and the daugh-
ter of a fashionable Moscow doctor. Their
early married life at Yasnaya Polyana was
tranquil. Family cares occupied the Countess,
and in the course of her life she bore thirteen
children, nine of whom survived infancy. Yet
she also acted as a copyist for her husband,
who after their marriage turned again to writ-
ing. He was soon at work upon "a novel of
the i8io's and *2o's" which absorbed all his
time and effort. He went frequently to Mos-
cow, "studying letters, diaries, and traditions"
and "accumulated a whole library" of histori-
cal material on the period. He interviewed
survivors of the battles of that time and trav-
eled to Borodino to draw up a map of the
battleground. Finally, in 1869, after his work
had undergone several changes in conception
and he had "spent five years of uninterrupted
andjgxceptionally strenuous labor Tnnierthe
IbesfcondUtions of life/' he published War and
Peace. Its appearance immediately established
Tolstoy's reputation, and in the judgment of
Turgenev, the acknowledged dean of Russian
letters, gave him "first place among all our
contemporary writers."

The years immediately following the com-
pletion of War and Peace were pa**efl in a
great variety of occupations, none of which
Tohtoy found satisfying. He tried busying



VI



BIOGRAPHICAL NOTE



himself with the affairs of his estate, under-
took the learning of Greek to read the ancient
classics, turned again to education, wrote a
series of elementary school books, and served
as school inspector. With much urging from
his wife and friends, he completed Anna Kare-
nina, which appeared serially between 1875
and 1877. Disturbed by what he considered his
unreflective and prosperous existence, Tolstoy
became increasingly interested in religion. At
first he turned to the orthodox faith of the
people. Unable to find rest there, he began a
detailed examination of religions, and out of
his reading, particularly of the Gospels, gradu-
ally evolved his own personal doctrine.

Following his conversion, Tolstoy adopted
a new mode of life. He dressed like a peasant,
devoted much of his time to manual work,
learned shoemaking, and followed a vegetari-
an diet. With the exception of his youngest
daughter, Alexandra, Tolstoy's family re-
mained hostile to his teaching. The breach be-
tween him and his wife grew steadily wider.
In 1879 he wrote the Kreutzer Sonata in which
he attacked the normal state of marriage and
extolled a life of celibacy and chastity. In 1881
he divided his estate among his heirs and, a
few years later, despite the opposition of his
wife, announced that he would forego royal-
ties on all the works published after his con-
version.

Tolstoy made no attempt at first to propa-
gate his religious teaching, although it attracted



many followers. After a visit to the Moscow
slums iri 1881, he became concerned with social
conditions, and he subsequently aided the suf-
ferers of the famine by sponsoring two hun-
dred and fifty relief kitchens. After his meet-
ing and intimacy with Chertkov, "Tolstoyism"
began to develop as an organized sect. Tol-
stoy's writings became almost exclusively pre-
occupied with religious problems. In addition
to numerous pamphlets and plays, he wrote
IV hat is Art? (1896), in which he explained
his new aesthetic theories, and Hadji-Murad,
(1904), which became the favorite work of his
old age. Although his activities were looked
upon with increasing suspicion by the official
authorities, Tolstoy escaped official censure
until 1901, when he was excommunicated by
the Orthodox Church. His followers were f re-
quently subjected to persecution, and many
were either banished or imprisoned.

Tolstoy's last years were embittered by
mounting hostility within his own household.
Although his personal life was ascetic, he felt
the ambiguity of his position as a preacher of
poverty living on his great estate. Finally, at
the age of eighty-two, with the aid of his daugh-
ter, Alexandra, he fled from home. His health
broke down a few days later, and he was re-
moved from the train to the station-master's
hut at Astopovo, where he died, November 7,
1910. He was buried at Yasnaya Polyana, in
the first public funeral to be held in Russia
without religious rites.



CONTEXTS



BIOGRAPHICAL NOTE v

The Principal Characters in War and Peace

Arranged in Family Groups xv

Dates of Principal Historical Events xvi

BOOK ONE

1-5. Anna Sche'rer's soiree i

6-3. Pierre at Prince Andrew's 1 1

9. Pierre at Anatole Kurdgin's. D61ok-

hov's bet 15

10. A name day at the Rost6vs' 18

11-1*4. Natasha and Boris 20

15. Anna Mikhdylovna and Bon's go to the

dying Count Beziikhov's 26

16. Pierre at his father's house; talks with

Boris 27

17. Countess Rost6va and Anna Mikhay-

lovna 30

18-19. Dinner at the Rost6vs'. Marya Dmitri-

cvna 31

20. S6nyaand Natasha. Nicholassings.The

Daniel Cooper 35

21. At Count Bczukhov's. Prince Vasfli and

Catiche 37

22-23. Anna Mikhdylovna and Pierre at Count

Bczukhov's 41

24. Anna Mikhdylovna and Catiche strug-

gle for the inlaid portfolio 45

25. Bald Hills. Prince N. A. Bolkonski.

Princess Mary's correspondence with

Julie Kardgina 47

26-27. Prince Andrew at Bald Hills 51

28. Prince Andrew leaves to join the army.

Princess Mary gives him an icon 55

BOOK TWO

1-2. Review near Braunau. Zherk6v and
D61okhov 60

3. Kutuzov and an Austrian general. ^Le

malheureux Mack. Zherk6v's fool-
ery 65

4. Nicholas and Denisov. Telydnin and

the missing purse 68

5. Nicholas in trouble with his fellow of-

ficers 72

6-8. Crossing the Enns. Burning the bridge.

Rost6v's baptism of fire 74

9. Prince Andrew sent with dispatches to

the Austrian court. The Minister of

War 81



10. Prince ( Andrew and Billbin 83

1 1. Hippolyte Kuragin and les ndtres 86

12. Prince Andrew received by the Emper-

or Francis. Bilibin's story of the Tha-
bor Bridge 87

13-14. Prince Andrew returns to Kutuzov.
Bagrati6n sent to Hollabriinn. …